----------------------------------------------------------------------------------------------
-- test 1 - basic
----------------------------------------------------------------------------------------------
drop FOREIGN TABLE tmp_data.sariel_dec_sariel
select *  from common.mdc_check_col_isdate('tmp_data', 'sariel_dec_sariel', 'birth_date', 1);  
select * from  common.usp_mdc_analyze_table ('tmp_data', 'sariel_dec_sariel');
select * from  common.usp_mdc_get_decrypted_data ('tmp_data', 'sariel_dec_sariel');

/*
CREATE FOREIGN TABLE tmp_data.sariel_dec_sariel (
  internalpatientid TEXT,
  birth_date TEXT,
  "encrypted_org_id" TEXT,
  "encrypted_date_shift_value" TEXT,
  prefix TEXT
)
SERVER srv_file_fdw
OPTIONS (
  filename 'C:\sariel\query-impala-basic.csv',
  format 'csv',
  header 'true');
*/
----------------------------------------------------------------------------------------------
-- test 2 - No date column
----------------------------------------------------------------------------------------------
drop FOREIGN TABLE tmp_data.sariel_dec_sariel
select * from  common.usp_mdc_analyze_table ('tmp_data', 'sariel_dec_sariel');
select * from  common.usp_mdc_get_decrypted_data ('tmp_data', 'sariel_dec_sariel');

/*
CREATE FOREIGN TABLE tmp_data.sariel_dec_sariel (
  internalpatientid TEXT,
  "encrypted_org_id" TEXT,
  "encrypted_date_shift_value" TEXT,
  prefix TEXT
)
SERVER srv_file_fdw
OPTIONS (
  filename 'C:\sariel\query-impala-no-date.csv',
  format 'csv',
  header 'true');
*/
----------------------------------------------------------------------------------------------
-- test 3 - No date shift value column
----------------------------------------------------------------------------------------------
drop FOREIGN TABLE tmp_data.sariel_dec_sariel
select * from  common.usp_mdc_analyze_table ('tmp_data', 'sariel_dec_sariel');
select * from  common.usp_mdc_get_decrypted_data ('tmp_data', 'sariel_dec_sariel');

/*
CREATE FOREIGN TABLE tmp_data.sariel_dec_sariel (
  internalpatientid TEXT,
  birth_date TEXT,
  "encrypted_org_id" TEXT,
  " d encrypted_date_shift_valu e" TEXT,
  prefix TEXT
)
SERVER srv_file_fdw
OPTIONS (
  filename 'C:\sariel\query-impala-basic.csv',
  format 'csv',
  header 'true');
*/
----------------------------------------------------------------------------------------------
-- test 4 - No date column + No date shift value column
----------------------------------------------------------------------------------------------
drop FOREIGN TABLE tmp_data.sariel_dec_sariel
select * from  common.usp_mdc_analyze_table ('tmp_data', 'sariel_dec_sariel');
select * from  common.usp_mdc_get_decrypted_data ('tmp_data', 'sariel_dec_sariel');

/*
CREATE FOREIGN TABLE tmp_data.sariel_dec_sariel (
  internalpatientid TEXT,
  "encrypted_org_id" TEXT,
  " e ncrypted_date_shift_valu e" TEXT,
  prefix TEXT
)
SERVER srv_file_fdw
OPTIONS (
  filename 'C:\sariel\query-impala-no-date.csv',
  format 'csv',
  header 'true');
*/
----------------------------------------------------------------------------------------------
-- test 5 - No patient enc col + No date column + No date shift value column
-- Expected: ERROR:  Error: No decryption columns
----------------------------------------------------------------------------------------------
drop FOREIGN TABLE tmp_data.sariel_dec_sariel
select * from  common.usp_mdc_analyze_table ('tmp_data', 'sariel_dec_sariel');
select * from  common.usp_mdc_get_decrypted_data ('tmp_data', 'sariel_dec_sariel');

/*
CREATE FOREIGN TABLE tmp_data.sariel_dec_sariel (
  internalpatientid TEXT,
  " e ncrypted_org_i d" TEXT,
  " e ncrypted_date_shift_valu e" TEXT,
  prefix TEXT
)
SERVER srv_file_fdw
OPTIONS (
  filename 'C:\sariel\query-impala-no-date.csv',
  format 'csv',
  header 'true');
*/
----------------------------------------------------------------------------------------------
-- test 6 - No patient enc col
----------------------------------------------------------------------------------------------
drop FOREIGN TABLE tmp_data.sariel_dec_sariel
select * from  common.usp_mdc_analyze_table ('tmp_data', 'sariel_dec_sariel');
select * from  common.usp_mdc_get_decrypted_data ('tmp_data', 'sariel_dec_sariel');

/*
CREATE FOREIGN TABLE tmp_data.sariel_dec_sariel (
  internalpatientid TEXT,
  birth_date TEXT,
  " e ncrypted_org_i d" TEXT,
  "encrypted_date_shift_value" TEXT,
  prefix TEXT
)
SERVER srv_file_fdw
OPTIONS (
  filename 'C:\sariel\query-impala-basic.csv',
  format 'csv',
  header 'true');
*/
----------------------------------------------------------------------------------------------
-- test 7 - No patient enc col
----------------------------------------------------------------------------------------------
drop FOREIGN TABLE tmp_data.sariel_dec_sariel
select * from  common.usp_mdc_analyze_table ('tmp_data', 'sariel_dec_sariel');
select * from  common.usp_mdc_get_decrypted_data ('tmp_data', 'sariel_dec_sariel');

/*
CREATE FOREIGN TABLE tmp_data.sariel_dec_sariel (
  birth_date TEXT,
  "encrypted_org_id" TEXT,
  "encrypted_date_shift_value" TEXT,
  prefix TEXT
)
SERVER srv_file_fdw
OPTIONS (
  filename 'C:\sariel\query-impala-no-internalpatioentid-column.csv',
  format 'csv',
  header 'true');
*/
----------------------------------------------------------------------------------------------
-- test 8 - Table has more than 1 Patient decryption column
----------------------------------------------------------------------------------------------
drop FOREIGN TABLE tmp_data.sariel_dec_sariel
select * from  common.usp_mdc_analyze_table ('tmp_data', 'sariel_dec_sariel');
select * from  common.usp_mdc_get_decrypted_data ('tmp_data', 'sariel_dec_sariel');

/*
CREATE FOREIGN TABLE tmp_data.sariel_dec_sariel (
  internalpatientid TEXT,
  birth_date TEXT,
  "c0 - encrypted_org_id" TEXT,
  "c1 - encrypted_org_id" TEXT,
  prefix TEXT
)
SERVER srv_file_fdw
OPTIONS (
  filename 'C:\sariel\query-impala-basic.csv',
  format 'csv',
  header 'true');
*/
----------------------------------------------------------------------------------------------
-- test 9 - Error: Table has more than 1 date shift decryption column
----------------------------------------------------------------------------------------------
drop FOREIGN TABLE tmp_data.sariel_dec_sariel
select * from  common.usp_mdc_analyze_table ('tmp_data', 'sariel_dec_sariel');
select * from  common.usp_mdc_get_decrypted_data ('tmp_data', 'sariel_dec_sariel');

/*
CREATE FOREIGN TABLE tmp_data.sariel_dec_sariel (
  internalpatientid TEXT,
  birth_date TEXT,
  "encrypted_org_id" TEXT,
  "c0 - encrypted_date_shift_value" TEXT,
  "c2 - encrypted_date_shift_value" TEXT
)
SERVER srv_file_fdw
OPTIONS (
  filename 'C:\sariel\query-impala-basic.csv',
  format 'csv',
  header 'true');
*/
----------------------------------------------------------------------------------------------
-- test 10 - Error: No decryption value provided
----------------------------------------------------------------------------------------------
drop FOREIGN TABLE tmp_data.miriam_ds
select * from  common.usp_mdc_analyze_table ('tmp_data', 'miriam_ds');
select * from  common.usp_mdc_get_decrypted_data('tmp_data', 'miriam_ds');

/*
CREATE FOREIGN TABLE tmp_data.miriam_ds (
  "Internal patient ID" TEXT,
  "Birth date" TEXT,
  "Deceased date" TEXT,
  "Deceased in hospital" TEXT,
  "gender" TEXT,
  "Reference Event-Diagnosis" TEXT,
  "Reference Event-Diagnosis date" TEXT,
  "Reference Event-Age at diagnosis" TEXT,
  "enc-encrypted_date_shift_value" TEXT,
  "enc-encrypted_org_id" TEXT,
  "hj-Admission start date-Days from Reference" TEXT,
  "hj-Length of stay" TEXT,
  "hj-Death time" TEXT,
  "hj-Diagnosis" TEXT,
  "hj-Admission end date" TEXT,
  "hj-ED reg time-Month Number" TEXT,
  "hj-Age at admission" TEXT,
  "hj-ED out time" TEXT,
  "hj-Discharged to" TEXT
)
SERVER srv_file_fdw
OPTIONS (
  filename 'C:\tmp_impala_csv_files\enc_test.csv',
  format 'csv',
  header 'true');
*/



select * from common.usp_mdc_create_foreign_table_with_origin_col_names (
  'C:\Dev\decryption\foreign_1.csv',
  'hierarchy_level_item_id,hierarchy_root_id,hd_0,hd_1,hd_2,hd_3,hd_4,hd_5,hd_6,hd_7,hd_8,hd_9,hd_10,hd_11,hd_12,hd_13,hd_14,hd_15,hd_16,hd_17,hd_18,hd_19',
  'sariel_1'
  );


select * from common.usp_mdc_create_foreign_table_with_origin_col_names (
  'C:\Dev\decryption\foreign_2.csv',
  '"Patie ntguid","dd","asv vvvvaf","as aF"',
  'sariel_2'
  );