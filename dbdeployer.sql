do $$
BEGIN

---------------------------------------------
----------------mdc_config-------------------
---------------------------------------------
IF NOT EXISTS (Select 1 from common.mdc_config where program = 'decryption' AND parameter_name = 'seed' ) THEN
    INSERT INTO common.mdc_config ("program", "parameter_name", "parameter_value", "prm_type")
    VALUES
    (E'decryption', E'seed', E'288', 0);
END IF;	

IF NOT EXISTS (Select 1 from common.mdc_config where program = 'decryption' AND parameter_name = 'date_shift_unit' ) THEN
    INSERT INTO common.mdc_config ("program", "parameter_name", "parameter_value", "prm_type")
    VALUES
    (E'decryption', E'date_shift_unit', E'week', 0);
END IF;	

IF NOT EXISTS (Select 1 from common.mdc_config where program = 'decryption' AND parameter_name = 'seed_date' ) THEN
    INSERT INTO common.mdc_config ("program", "parameter_name", "parameter_value", "prm_type")
    VALUES
    (E'decryption', E'seed_date', E'2021-05-30 00:00:00', 0);
END IF;	

IF NOT EXISTS (Select 1 from common.mdc_config where program = 'decryption' AND parameter_name = 'app_version' ) THEN
    INSERT INTO common.mdc_config ("program", "parameter_name", "parameter_value", "prm_type")
    VALUES
    (E'decryption', E'app_version', E'5', 0);
END IF;	

IF NOT EXISTS (Select 1 from common.mdc_config where program = 'decryption' AND parameter_name = 'calc_tmp_num' ) THEN
    INSERT INTO common.mdc_config ("program", "parameter_name", "parameter_value", "prm_type")
    VALUES
    (E'decryption', E'calc_tmp_num', E'7', 0);
END IF;	

END;
$$;


---------------------------------------------
----------------usp_mdc_create_foreign_table_with_origin_col_names-------------------
---------------------------------------------
DROP FUNCTION IF EXISTS common.usp_mdc_create_foreign_table_with_origin_col_names(v_full_file_name text, v_cols text, v_target_foreign_table varchar);
CREATE OR REPLACE FUNCTION common.usp_mdc_create_foreign_table_with_origin_col_names (
  v_full_file_name text,
  v_cols text,
  v_target_foreign_table varchar
)
RETURNS text AS
$body$
declare

  v_schema varchar;
  v_cols_str text;
  v_row_count integer; 
  v_sql text;
  rec varchar;	

begin

	v_schema := 'tmp_data';
	set schema 'tmp_data';

	/*  debug  
	---------------    
	raise info 'full file path = %', v_full_file_name;
	return 1;
	EXIT; 
	-------------*/
	
	v_cols_str := '';
	
	-- add columns to the table
	for rec in 
    	select unnest(string_to_array(v_cols, ','))    			
	loop
		v_cols_str := v_cols_str || rec::varchar || ' text, ';        
	end loop;
	
	v_cols_str := substring(v_cols_str, 1, length(v_cols_str)-2);


	/*  debug 
	---------------
	raise info 'v_cols_str = %', v_cols_str;    
	return 1;
	EXIT;
	-------------*/    

	execute format('drop foreign table if exists %s', v_target_foreign_table);	    


	/*  debug 
	--------------- 
	v_sql := 'create foreign table IF NOT EXISTS ' || v_target_foreign_table || '(' || v_cols_str || ') server srv_file_fdw options ( filename ''' || v_full_file_name || ''', format ''csv'' , header ''true'')';
	raise info 'the sql statement = %', v_sql;
	return 1;
	EXIT;
	-------------*/
	
	/* execute format('create foreign table IF NOT EXISTS %s(%s) server srv_file_fdw options ( filename ''%s'', format ''csv'' , header ''true'')', v_target_foreign_table, v_cols_str, v_full_file_name); */
	
	BEGIN
		execute format('create foreign table IF NOT EXISTS %s(%s) server srv_file_fdw options ( filename ''%s'', format ''csv'' , header ''true'')', v_target_foreign_table, v_cols_str, v_full_file_name);
			/*  debug 
          --------------- 
          v_sql := 'create foreign table IF NOT EXISTS ' || v_target_foreign_table || '(' || v_cols_str || ') server srv_file_fdw options ( filename ''' || v_full_file_name || ''', format ''csv'' , header ''true'')';
          raise info 'the sql statement = %', v_sql;
          return 1;
          EXIT;
         -------------*/
        v_sql := 'select count(1) from ' || v_target_foreign_table;			 
        execute v_sql into v_row_count; 
		EXCEPTION
			when others then
			BEGIN
				execute format('create foreign table IF NOT EXISTS %s(%s) server srv_file_fdw options ( filename ''%s'', format ''csv'' , header ''true'', encoding ''SQL_ASCII'')', v_target_foreign_table, v_cols_str, v_full_file_name);
				v_sql := 'select count(1) from ' || v_target_foreign_table;				
                execute v_sql into v_row_count;
				EXCEPTION
					when others then
						execute format('create foreign table IF NOT EXISTS %s(%s) server srv_file_fdw options ( filename ''%s'', format ''csv'' , header ''true'', encoding ''windows-1255'')', v_target_foreign_table, v_cols_str, v_full_file_name);
						v_sql := 'select count(1) from ' || v_target_foreign_table;
                        execute v_sql into v_row_count;
				
			END;  		
	END;    

	return 'tmp_data.' || v_target_foreign_table;
	
	exception when others then 

	execute format('drop foreign table if exists %s', v_target_foreign_table);		

	raise notice '% %', SQLERRM, SQLSTATE;        
    set log_error_verbosity = verbose; 
	return 0;

end;
$body$
LANGUAGE 'plpgsql'
VOLATILE
CALLED ON NULL INPUT
SECURITY INVOKER
COST 100;

---------------------------------------------
----------------mdc_num_decryption-----------
---------------------------------------------
DROP FUNCTION IF EXISTS common.mdc_num_decryption(v_seed integer, v_value varchar, v_apply_date_shift boolean, v_date_shift_unit varchar, v_org_date_value varchar);
CREATE OR REPLACE FUNCTION common.mdc_num_decryption (
  v_seed integer,
  v_value varchar,
  v_apply_date_shift boolean = false,
  v_date_shift_unit varchar = NULL::character varying,
  v_org_date_value varchar = NULL::character varying
)
RETURNS varchar AS
$body$
declare
v_decrypted_value 			bigint;
v_calc_tmp_num				integer;
v_decrypted_result_date  	timestamp;
v_sql text;

begin
	if v_value is null then
    	raise 'Error: Some of the patients have an empty decryption value';
    end if;	
    
    ------------------
    --get calc_tmp_num
    ------------------
    select 	parameter_value into v_calc_tmp_num 
    from 	common.mdc_config
    where	program = 'decryption' and parameter_name = 'calc_tmp_num';
    --calc_tmp_num validation
    if v_calc_tmp_num is null then
        raise 'Error: No calc_tmp_num definition in config table';
    end if;
	
	--------
	--debug
	--------
    --raise info 'v_calc_tmp_num = %', v_calc_tmp_num;

	v_decrypted_value := (exp(v_value::numeric)*v_seed)-v_calc_tmp_num;
	
    if v_apply_date_shift = TRUE then    
    	if (v_date_shift_unit is null) or ((v_date_shift_unit <> 'week') and (v_date_shift_unit <> 'day')) then
        	raise 'Error: No v_date_shift_unit or v_org_date_value provided. The date shift unit possible values: week/day';
        end if;
    	
        if v_org_date_value is null then
        	return null;
        end if;
        
		--------
		--debug
		--------
		--raise info 'v_org_date_value = %', v_org_date_value;
        --raise info 'v_decrypted_value = %', v_decrypted_value;
        --raise info 'v_date_shift_unit = %', v_date_shift_unit;
        
        v_sql := 'select ''' || v_org_date_value || '''::timestamp - (''' || v_decrypted_value::varchar || ' ' || v_date_shift_unit || '''' || ')::interval';
        
		--------
		--debug
		--------
		--raise info 'v_sql = %', v_sql;
        
		execute v_sql into v_decrypted_result_date;
        return v_decrypted_result_date;
        
        --------
		--debug
		--------
		--v_decrypted_value := v_org_date_value::timestamp + (v_decrypted_value::varchar || ' ''' || v_date_shift_unit || '''')::interval;
        --v_decrypted_value := v_sql;
        --raise info 'v_decrypted_value = %', v_decrypted_value;
		
    end if;
    
    return v_decrypted_value;
end;
$body$
LANGUAGE 'plpgsql'
VOLATILE
CALLED ON NULL INPUT
SECURITY INVOKER
COST 100;

ALTER FUNCTION common.mdc_num_decryption (v_seed integer, v_value varchar, v_apply_date_shift boolean, v_date_shift_unit varchar, v_org_date_value varchar)
  OWNER TO postgres;

-------------------------------------------------
----------------usp_mdc_analyze_table------------
-------------------------------------------------
DROP FUNCTION IF EXISTS common.usp_mdc_analyze_table(v_schema varchar, v_table varchar);
CREATE OR REPLACE FUNCTION common.usp_mdc_analyze_table (
  v_schema varchar,
  v_table varchar
)
RETURNS text [] AS
$body$
declare
rec 				 varchar;
v_is_date 			 varchar = '';
v_is_date_query 	 varchar = '';
v_count				 integer;
v_patientCol_count   integer;
v_dateShiftCol_count integer;
v_all_cols 			 text = '';
v_date_cols 		 text = '';
v_enc_patient_col	 varchar = '';
v_enc_dateshift_col  varchar = '';
v_sql 				 text;
v_patient_col_name 	 varchar = 'encrypted_id';
v_dateshift_col_name varchar = 'encrypted_date_shift_value';
v_sample_size		 integer = 300;
result				 text[] = null;		

begin

-----------------------
--Check if table exists
-----------------------
select 	1 into v_count
from 	information_schema.columns
where 	table_name = v_table
and		table_schema = v_schema
and		table_catalog = 'mdclone';

if v_count is null then
	raise 'Error: Table %.% not found',v_schema,v_table ;
end if;

-------------------------
--Start loop over cloumns
-------------------------
for rec in
 	select 	column_name 
	from 	information_schema.columns
     where 	table_name = v_table
     and	table_schema = v_schema
     and	table_catalog = 'mdclone'
 loop
     
     v_all_cols := v_all_cols || '"' || rec || '",';
    
 	-------------
    --dates
    -------------
     -- Checkong if all column is null or empty string
 	  v_sql := 'select count(1)- (select count(1) from ' || v_schema || '.' || v_table || ' where "' || rec || '" is null or replace("' || rec || '"::TEXT, '' '' ,'''') = '''') from '|| v_schema || '.' || v_table;
      execute v_sql into v_count;   
      --v_count := 1;   
      v_is_date := '';
      if v_count <> 0 THEN
      		v_is_date_query := 'select "' || rec || '" from ' || v_schema || '.' || v_table || ' where "' || rec || '" is not null and replace("' || rec || '"::TEXT, '' '' ,'''') <> ''''' || ' limit ' || v_sample_size;
      		select * into v_is_date from common.mdc_check_col_isdate('', 
      		                                                         '', 
                                                                     '"' || rec || '"', 
                                                                     1, 
                                                                     v_is_date_query
                                                                     );                     			
      else
      		v_is_date := 'false';
      end if;
      
      if lower(v_is_date) <> 'false' then     
      		v_date_cols := v_date_cols || '"' || rec || '",';
      end if;
		
      -------------------
      --v_enc_patient_col
      -------------------
      if strpos(rec, v_patient_col_name) > 0 then
          v_enc_patient_col := v_enc_patient_col || '"' || rec || '",';
      end if;

      ---------------------
      --v_enc_dateshift_col
      ---------------------
      if strpos(rec, v_dateshift_col_name) > 0 then
          v_enc_dateshift_col := v_enc_dateshift_col || '"' || rec || '",';
      end if;    
       
 end loop;
 
v_all_cols  := trim(trailing ',' from v_all_cols);
v_date_cols := trim(trailing ',' from v_date_cols);
v_enc_patient_col := trim(trailing ',' from v_enc_patient_col);
v_enc_dateshift_col := trim(trailing ',' from v_enc_dateshift_col);

-------------------------------------------
--Check decryption columns - if more than 1
-------------------------------------------
v_patientCol_count := 0;
select count(1) into v_patientCol_count
from
(select unnest(string_to_array(v_enc_patient_col::text, ',')))t;

if v_patientCol_count > 1 then
	raise 'Error: Table has more than 1 Patient decryption column';
end if;

v_dateShiftCol_count := 0;
select count(1) into v_dateShiftCol_count
from
(select unnest(string_to_array(v_enc_dateshift_col::text, ',')))t;

if v_dateShiftCol_count > 1 then
	raise 'Error: Table has more than 1 date shift decryption column';
end if;

-------------------------------------------
----check if decryption columns exist
-------------------------------------------
if (v_patientCol_count = 0) and (v_dateShiftCol_count = 0) then
	raise 'Error: No decryption columns provided.';
end if;

/*
raise info 'v_all_cols = %', v_all_cols;
raise info 'v_date_cols = %', v_date_cols;
raise info 'v_enc_patient_col = %', v_enc_patient_col;
raise info 'v_enc_dateshift_col = %', v_enc_dateshift_col;
*/

result := ARRAY(select v_all_cols union all
			 select v_date_cols union all
             select v_enc_patient_col union all
             select v_enc_dateshift_col);

--raise info 'result = %', result;             
RETURN result;             

end;
$body$
LANGUAGE 'plpgsql'
VOLATILE
CALLED ON NULL INPUT
SECURITY INVOKER
COST 100;

ALTER FUNCTION common.usp_mdc_analyze_table (v_schema varchar, v_table varchar)
  OWNER TO postgres;


-------------------------------------------------
----------------usp_mdc_get_decrypted_data------------
-------------------------------------------------
DROP FUNCTION IF EXISTS common.usp_mdc_get_decrypted_data(v_schema varchar, v_table varchar, v_session_history_id bigint);
CREATE OR REPLACE FUNCTION common.usp_mdc_get_decrypted_data (
  v_schema varchar,
  v_table varchar,
  v_session_history_id bigint = NULL::bigint
)
RETURNS text AS
$body$
declare

v_seed integer;
v_seed_date varchar;
v_date_shift_unit varchar;
v_sql text;
v_patient_query varchar = '';
v_date_query text = '';
v_patient_col_alias varchar = '"Decrypted patient id"';
v_columns text[] = null;
v_all_cols text;
v_date_cols text;
v_enc_patient_col varchar;
v_enc_dateshift_col varchar;
v_calculation_time timestamp;
v_session_id bigint;

begin

---------------------
----Input validations
---------------------
----check existence of table and schema
if (v_schema = '') or (v_table = '') then
	raise 'Error: No schema or table provided.';
end if;

----check if session id received from input
if v_session_history_id is null then
	raise 'Error: No Session History id provided';
end if;

----------------------------------------
--get session_id from session_history_id
----------------------------------------
select session_id into v_session_id
from public.mdc_session_history
where session_history_id = v_session_history_id;

if v_session_id is null then 
	raise 'Error: No Session_id found for session_history_id = %', v_session_history_id;
end if;

-----------
--get seed
-----------
select 	parameter_value into v_seed 
from 	common.mdc_config
where	program = 'decryption' and parameter_name = 'seed';
--seed validation
if v_seed is null then
	raise 'Error: No seed definition in config table';
end if;
--raise info 'v_seed = %', v_seed;

---------------
--get seed date
---------------
select 	parameter_value into v_seed_date 
from 	common.mdc_config
where	program = 'decryption' and parameter_name = 'seed_date';
--seed date validation
if (v_seed_date is null) or (trim(v_seed_date) = '') then
	raise 'Error: No seed date definition in config table';
end if;
--raise info 'v_seed = %', v_seed;

--------------------------------------------
--get calculation_time from encryption_table
--------------------------------------------
select 		min(muse.calculation_time) into v_calculation_time
from 		public.mdc_user_session_event muse
inner join 	public.mdc_site_event_info sei
on 			muse.event_id = sei.event_id 
where 		sei.event_table_name ='encryption_table'
and 		muse.session_id = v_session_id;

if v_calculation_time is null then
	raise 'Error: No calculation_time found in mdc_user_session_event table, for session id = %', v_session_id;
end if;

---------------------------------------------
-- Check Seed date Vs. event calculation_time
---------------------------------------------
if v_seed_date::TIMESTAMP > v_calculation_time THEN
	raise 'Error: Seed updated date, % , is older than encryption_table calculation time, %. Event regenerate is required.', v_seed_date, v_calculation_time;
end if;


select * into v_columns from  common.usp_mdc_analyze_table (v_schema, v_table);
v_all_cols := v_columns[1];
v_date_cols := v_columns[2];
v_enc_patient_col := v_columns[3];
v_enc_dateshift_col := v_columns[4];

-----------------------------------------------------------------
-- Remove v_enc_patient_col + v_enc_dateshift_col from v_all_cols 
-----------------------------------------------------------------
if strpos(v_all_cols, ',' || v_enc_patient_col) > 0 then
    v_all_cols := replace(v_all_cols, ',' || v_enc_patient_col, '');
elseif strpos(v_all_cols, v_enc_patient_col || ',') > 0 then
    v_all_cols := replace(v_all_cols, v_enc_patient_col || ',', '');
end if;


if strpos(v_all_cols, ',' || v_enc_dateshift_col) > 0 then
    v_all_cols := replace(v_all_cols, ',' || v_enc_dateshift_col, '');
elseif strpos(v_all_cols, v_enc_dateshift_col || ',') > 0 then
    v_all_cols := replace(v_all_cols, v_enc_dateshift_col || ',', '');
end if;

/*
raise info 'v_all_cols = %', v_all_cols;
raise info 'v_date_cols = %', v_date_cols;
raise info 'v_enc_patient_col = %', v_enc_patient_col;
raise info 'v_enc_dateshift_col = %', v_enc_dateshift_col;
*/

---------------------
--Patient decryption
---------------------
if v_enc_patient_col <> '' then
	v_patient_query := ', common.mdc_num_decryption(' || v_seed || ',' || v_enc_patient_col || ') as ' || v_patient_col_alias;
end if;
--raise info 'v_patient_query = %', v_patient_query;

---------------------
--get date shift unit
---------------------
if (v_enc_dateshift_col <> '') and (v_date_cols <> '') then
		--get date shift unit
		select 	parameter_value into v_date_shift_unit 
        from 	common.mdc_config
        where	program = 'decryption' and parameter_name = 'date_shift_unit';
        
        --v_date_shift_unit validation
        if v_date_shift_unit is null then
            raise 'Error: No v_date_shift_unit definition in mdc_config';
        end if;
        --raise info 'v_date_shift_unit = %', v_date_shift_unit;
else
	v_enc_dateshift_col := '';
    v_date_cols := ''; 
end if;

-----------------------------------
--all columns + Date columns bulid
-----------------------------------        
--build date column select 
with all_cols as (
select row_number() over() rn, tmp2.c_all
from
(select regexp_split_to_table(all_cols::text,E',') c_all
from
(select v_all_cols all_cols)tmp1)tmp2)
, date_cols as (
select tmp4.d_all
from
(select regexp_split_to_table(date_cols::text,E',') d_all
from
(select v_date_cols date_cols)tmp3)tmp4)
select string_agg(bb,',' order by rn) into 	v_date_query
from (
select 	rn,
        c_all, 
        d_all, 
        case 	when d_all is not null then 
                'common.mdc_num_decryption(' || v_seed || ',' || v_enc_dateshift_col || ', true, ''' || v_date_shift_unit || ''',' || c_all || ') as ' || trim(trailing '"' from c_all) || '_decrypted"'
                else c_all
        end as bb
from 	all_cols
left outer join date_cols
on 		trim(all_cols.c_all) = trim(date_cols.d_all)
) q;    

--raise info 'v_date_query = %', v_date_query;

----------------------------
-- Building the final query
----------------------------
v_sql := 'select ' || v_date_query || v_patient_query || ' from ' || v_schema || '.' || v_table;
--raise info 'v_sql = %', v_sql;

return v_sql;

end;
$body$
LANGUAGE 'plpgsql'
VOLATILE
CALLED ON NULL INPUT
SECURITY INVOKER
COST 100;

ALTER FUNCTION common.usp_mdc_get_decrypted_data (v_schema varchar, v_table varchar, v_session_history_id bigint)
  OWNER TO postgres;